/*
=====>>>>>>>>>> LOGIC FLOW <<<<<<<<<<<<=====
Hendrick =>
    - Penamaan file perlu diganti
    • Pseudo 001.js
        - tidak ada "START IF"
        - dibagian PRINT, bisa ditambahkan "Nilai" + nama
    • Pseudo 002.js
        + THEN (bisa dipindah kedalam)
        - tidak ada "START IF"
        - DISPLAY bisa diganti PRINT - agar konsisten
    •  Pseudo 001.png
        - Chart "Baca Nilai" tidak diperlukan
    • Pseudo 002.png
        + keterangan TRUE FALSE on arrow symbol
        - Chart "Read huruf pertama nama"

Tarri =>
    - bisa reu-upload dengan menggunakan account sendiri
    • FlowChart01.jpg
        - Chart "Read" dan "Process" bisa dihapus
    • FlowChart02.jpg
        -  Chart "Read" dan "Process" bisa dihapus
        - on decision symbol "Name[0]" bisa diperjelas lagi
    • Pseudocode02.js
        - valuenya bisa di set duluan
        - "name[0]" bisa diperjelas lagi
        - "equal to" bisa ditambahkan OR

Yopy =>
    • flowchart001.jpg
        - perbaiki penggunaan chart symbol pada "input nama dan nilai"
        - akan lebih baik ditambah OR pada decision symbol
    • pseudo01.js
        - nama variable tidak ada
        - pada "set variable" bisa ditambahkan "with value"
        - penulisan "start conditional" bisa di sesuaikan dengan "end conditional"
        - line 34: <= >= bisa diganti dengan "less than or equals" atau "more than or equals"
        - penulisan "set" dan "if" bisa disamakan kapitalize semua
        - pada kondisi else, seharusnya tidak ada kondisi
    • pseudo02.js
        - nama variable tidak ada
        - pada "set variable" bisa ditambahkan "with value"
        - pada kondisi "name[0]" bisa diperjelas
        - tambahkan "OR" pada a, i, u, e, o

Dana =>
    - nama file bisa diperjelas
    - bisa menggunakan bahasa inggris
    • 001.js
        - bisa ditambahkan "START CONDITIONAL" dan "END CONDITIONAL"
        - "ELSEIF" perlu ada spasi
        - bisa dijadikan satu line aja, kalau masih dalam kondisi: line 65
    • 002.js
        - "set nama" bisa diganti  "SET VARIABLE "name" WITH VALUE "Ardi""
        - bisa ditambahkan "START CONDITIONAL" dan "END CONDITIONAL"

Yohan =>
    - nama file bisa diperjelas
    - bisa menggunakan bahasa inggris
    • OOH-1.jpg
        - arrow symbolnya bisa diperbaiki
    • OOH-1.txt
        - untuk declarasi variable bisa dibikin satu satu "SET VARIABLE "name" WITH VALUE "ardi""
        - symbol >= <=, bisa diganti dengan "less than or equals", otherwise..
        - IF bisa diganti dengan menggunakan ELSE IF
        - bisa ditambahkan "START CONDITIONAL" dan "END CONDITIONAL"
        - bisa ditambahkan "START PROGRAM" dan "END PROGRAM"
    • OOH-2.jpg
        - arrow symbolnya bisa diperbaiki
        - [0] pada decision symbol bisa diperjelas 
        - Chart "tampilkan" bisa diganti menggunakan jajargenjang
    • OOH-2.txt
        - untuk declarasi variable bisa dibikin satu satu "SET VARIABLE "name" WITH VALUE "ardi""
        - Identasi perlu diperhatikan
        - tidak perlu ada spasi antar kondisi
        - bisa ditambahkan "START CONDITIONAL" dan "END CONDITIONAL"
        - bisa ditambahkan "START PROGRAM" dan "END PROGRAM"

Adnin =>
    - nama file bisa diperjelas
    • LF_OH#1_001.js
        - pada varibale declaration bisa ditambahkan "WITH VALUE "value""
        - symbol >= <=, bisa diganti dengan "less than or equals", otherwise..
        - penggunaan keyword bisa menggunakan kapitalize
        - "SET A", bisa diperjelas
    • LF_OH#1_002.js
        - line 31: "huruf pertama" dari apa?
        - line 31: bisa ditambahkan "OR"
        - penggunaan keyword bisa menggunakan kapitalize
    • flowchart_OH#1_002.png
        - pada decision symbol, bisa diperjelas dengan menggunakan "OR"

Ibed =>
    - untuk share link, bisa mneggunakan tiny url agar lebih pendek
    • pseudo code(1).js
        - untuk declarasi variable bisa dibikin satu satu "SET VARIABLE "name" WITH VALUE "ardi""
        - bisa ditambahkan "START CONDITIONAL" dan "END CONDITIONAL"
        - tidak perlu {}
        - ELSE IF bisa dijadikan satu line aja
        - penggunaan "more than equal" bisa dijadikan "more than or equal"
        - pada saat set value, sebaiknya ditambahkan "SET "desc" to "A""
    • pseudo code(2).js
        - bisa ditambahkan "START CONDITIONAL" dan "END CONDITIONAL"
        - bisa ditambahkan "START PROGRAM" dan "END PROGRAM"
        - ELSE IF bisa dijadikan satu line aja
        - untuk declarasi variable bisa dibikin satu satu "SET VARIABLE "name" WITH VALUE "ardi""
        • Flowchart
            - array[0] bisa diperjelas maksudnya
          
Kamal =>
    - Flowchartnya bisa langsung dikasih link aja
    - Pseudocode bisa diperjelas lagi    
*/















































